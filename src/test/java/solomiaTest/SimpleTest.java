package solomiaTest;

import com.solomia.TypeEntity;
import com.solomia.connector.DBManager;
import com.solomia.constant.Constants;
import com.solomia.factory.MyFactory;
import com.solomia.factory.ServiceFactory;
import com.solomia.services.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SimpleTest {
    private static Logger logger = LogManager.getLogger(SimpleTest.class);
    private static Connection connection = null;
    @BeforeEach
    void before() {
        System.out.println("Before each test");
    }
    @Test
    void checkConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(Constants.url, Constants.user, Constants.password);
            } catch (SQLException e) {
                logger.error("SQLException: " + e.getMessage());
                logger.error("SQLState: " + e.getSQLState());
                logger.error("VendorError: " + e.getErrorCode());
            }
        }
        try {
            if(!connection.isClosed()) {
                logger.info("Success");
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    @Test
    void checkSelectById() throws SQLException {
        ServiceFactory serviceFactory = new MyFactory();
        Service servicePayment = serviceFactory.createService(TypeEntity.PAYMENT);
        if(servicePayment.selectById(1)){
            logger.info("Test is successful");
        } else {
            logger.error("Do not exist by id");
        }
    }

    @AfterEach
    void after() {
        System.out.println('\n'+"After each test");
    }
}
