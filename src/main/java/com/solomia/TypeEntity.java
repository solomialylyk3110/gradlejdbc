package com.solomia;

public enum TypeEntity {
    CLIENT, CONTRACT, OPERATION, PAYMENT, REALTOR
}
