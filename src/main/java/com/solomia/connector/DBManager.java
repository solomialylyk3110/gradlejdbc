package com.solomia.connector;

import com.solomia.constant.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
    private static Logger logger = LogManager.getLogger(DBManager.class);
    private static Connection connection = null;

    private DBManager() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(Constants.url, Constants.user, Constants.password);
            } catch (SQLException e) {
                logger.error("SQLException: " + e.getMessage());
                logger.error("SQLState: " + e.getSQLState());
                logger.error("VendorError: " + e.getErrorCode());
            }
        }
        try {
            if(!connection.isClosed()) {
                logger.info("Success");
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return connection;
    }
}
