package com.solomia;

import com.solomia.factory.MyFactory;
import com.solomia.factory.ServiceFactory;
import com.solomia.services.Service;

import java.sql.SQLException;

public class Main {
        public static void main(String[] args) throws SQLException {
            ServiceFactory serviceFactory = new MyFactory();
            Service serviceRealtor = serviceFactory.createService(TypeEntity.REALTOR);
            serviceRealtor.select();

            Service servicePayment = serviceFactory.createService(TypeEntity.PAYMENT);
            //servicePayment.create(new Payment(5, 4000, 48000));
            servicePayment.select();
            //servicePayment.delete(5);
            //servicePayment.select();
            // Join join = new ClientJoinContract();
            //join.join();

    }
}
