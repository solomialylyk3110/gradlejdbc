package com.solomia.query.join;

import java.sql.SQLException;

public interface Join{
    void join() throws SQLException;
}
