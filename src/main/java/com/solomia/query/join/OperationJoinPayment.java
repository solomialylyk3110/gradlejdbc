package com.solomia.query.join;

import com.solomia.connector.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class OperationJoinPayment implements Join {
    private static Logger logger = LogManager.getLogger(OperationJoinPayment.class);
    private static final String OPERATIONPAYMENT = "select operation.operation_name, payment.payment_month " +
            "from operation inner join contract inner join payment " +
            "on operation.operation_id = contract.operation_id and contract.payment_id= payment.payment_id;";

    public void join() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(OPERATIONPAYMENT);
        ResultSet resultSet = ps.executeQuery();
        Map<String, String> stringMap = new HashMap<>();
        while (resultSet.next()) {
            stringMap.put(resultSet.getString("operation_name"),
                    resultSet.getString("payment_month"));
        }
        logger.info(stringMap);
    }
}
