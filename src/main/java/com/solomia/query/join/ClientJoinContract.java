package com.solomia.query.join;

import com.solomia.connector.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientJoinContract implements Join {
    private static Logger logger = LogManager.getLogger(ClientJoinContract.class);
    private static final String CLIENTCONTRACT = "select client_name, contract.contract_id from `client` " +
            "inner join client_contract inner join  contract\n" +
            "on `client`.client_id= client_contract.client_id and client_contract.contract_id=contract.contract_id;";

    public void join() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(CLIENTCONTRACT);
        ResultSet resultSet = ps.executeQuery();
        //Map<String, String> stringMap = new HashMap<>();
        List<String> list = new ArrayList<>();
        while (resultSet.next()) {
            list.add(resultSet.getString("client_name") + " - "
                    + resultSet.getString("contract_id"));
        }
        logger.info(list);
    }
}
