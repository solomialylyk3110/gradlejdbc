package com.solomia.query;

import com.solomia.services.*;

import java.sql.SQLException;

public class AdvancedQuery {

    public void deleteFullRealtor(int id) throws SQLException {
        new ContractService().deleteByRealtor(id);
        new RealtorService().delete(id);
    }

    public void deleteFullPayment(int id) throws SQLException {
        new ContractService().deleteByPayment(id);
        new PaymentService().delete(id);
    }

    public void deleteFullOperation(int id) throws SQLException {
        new ContractService().deleteByOperation(id);
        new OperationService().delete(id);
    }

    public void deleteFullClient(int id) throws SQLException {
        new ClientContractService().deleteByClient(id);
        new ClientService().delete(id);
    }

    void deleteFullContract(int id) throws SQLException {
        new PaymentService().deleteByContract(id);
        new OperationService().deleteByContract(id);
        new RealtorService().deleteByContract(id);
        new ClientContractService().deleteByContract(id);
    }

}
