package com.solomia.model;

public class Payment {
    private Integer paymentId;
    private Integer paymentMonth;
    private Integer paymentYear;

    public Payment(int paymentId, int paymentMonth, int paymentYear) {
        this.paymentId = paymentId;
        this.paymentMonth = paymentMonth;
        this.paymentYear = paymentYear;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getPaymentMonth() {
        return paymentMonth;
    }

    public void setPaymentMonth(int paymentMonth) {
        this.paymentMonth = paymentMonth;
    }

    public int getPaymentYear() {
        return paymentYear;
    }

    public void setPaymentYear(int paymentYear) {
        this.paymentYear = paymentYear;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "paymentId=" + paymentId +
                ", paymentMonth=" + paymentMonth +
                ", paymentYear=" + paymentYear +
                '}';
    }
}
