package com.solomia.model;

import java.sql.Date;

public class Contract {
    private Integer contractId;
    private Date dateSigning;
    private Date dateEnding;
    private Integer paymentId;
    private Integer realtorId;
    private Integer operationId;

    public Contract(Integer contractId, Date dateSigning, Date dateEnding, Integer paymentId, Integer realtorId, Integer operationId) {
        this.contractId = contractId;
        this.dateSigning = dateSigning;
        this.dateEnding = dateEnding;
        this.paymentId = paymentId;
        this.realtorId = realtorId;
        this.operationId = operationId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Date getDateSigning() {
        return dateSigning;
    }

    public void setDateSigning(Date dateSigning) {
        this.dateSigning = dateSigning;
    }

    public Date getDateEnding() {
        return dateEnding;
    }

    public void setDateEnding(Date dateEnding) {
        this.dateEnding = dateEnding;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getRealtorId() {
        return realtorId;
    }

    public void setRealtorId(Integer realtorId) {
        this.realtorId = realtorId;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "contractId=" + contractId +
                ", dateSigning=" + dateSigning +
                ", dateEnding=" + dateEnding +
                ", paymentId=" + paymentId +
                ", realtorId=" + realtorId +
                ", operationId=" + operationId +
                '}' + '\n';
    }
}
