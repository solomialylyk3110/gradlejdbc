package com.solomia.model;

public class ClientContract {
    private Integer clientId;
    private Integer contractId;

    public ClientContract(Integer clientId, Integer contractId) {
        this.clientId = clientId;
        this.contractId = contractId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Override
    public String toString() {
        return "ClientContract{" +
                "clientId=" + clientId +
                ", contractId=" + contractId +
                '}';
    }
}
