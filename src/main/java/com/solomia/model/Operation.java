package com.solomia.model;

public class Operation {
    private Integer operationId;
    private String operationName;

    public Operation(int operationId, String operationName) {
        this.operationId = operationId;
        this.operationName = operationName;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "operationId=" + operationId +
                ", operationName='" + operationName + '\'' +
                '}';
    }
}
