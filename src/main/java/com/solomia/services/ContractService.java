package com.solomia.services;

import com.solomia.connector.DBManager;
import com.solomia.model.Contract;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContractService implements Service<Contract> {
    private static Logger logger = LogManager.getLogger(ContractService.class);
    private static final String CREATE = "INSERT INTO contract VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE contract SET data_signing=?, data_ending=?, payment_id=?, realtor_id=?, operation_id=? WHERE contract_id=?";
    private static final String DELETE = "DELETE FROM contract WHERE contract_id=?";
    private static final String GET_ALL = "SELECT * FROM contract";
    private static final String SELECT = "SELECT * FROM contract where contract_id=?";
    private static final String DELETEBYREALTOR= "DELETE FROM contract WHERE realtor_id=? ";
    private static final String DELETEBYPAYMENT= "DELETE FROM contract WHERE payment_id=? ";
    private static final String DELETEBYOPERATION= "DELETE FROM contract WHERE operation_id=? ";

    @Override
    public void create(Contract entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getContractId());
            ps.setDate(2,entity.getDateSigning());
            ps.setDate(3,entity.getDateEnding());
            ps.setInt(4, entity.getPaymentId());
            ps.setInt(5, entity.getRealtorId());
            ps.setInt(6, entity.getOperationId());
            ps.executeUpdate();
        }
    }

    public void update(Contract entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getContractId());
            ps.setDate(2,entity.getDateSigning());
            ps.setDate(3,entity.getDateEnding());
            ps.setInt(4, entity.getPaymentId());
            ps.setInt(5, entity.getRealtorId());
            ps.setInt(6, entity.getOperationId());
            ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteByRealtor(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYREALTOR)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteByOperation(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYOPERATION)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteByPayment(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYPAYMENT)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }


    @Override
    public void select() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(GET_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Contract> contractList = new ArrayList<>();
        while (resultSet.next()) {
            contractList.add(new Contract(resultSet.getInt("contract_id"),
                    resultSet.getDate("data_signing"), resultSet.getDate("data_ending"),
                    resultSet.getInt("payment_id"), resultSet.getInt("realtor_id"), resultSet.getInt("operation_id")));
        }
        logger.info(contractList);
    }

    public boolean selectById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(SELECT);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        List<Contract> contractList = new ArrayList<>();
        while (resultSet.next()) {
            contractList.add(new Contract(resultSet.getInt("contract_id"),
                    resultSet.getDate("data_signing"), resultSet.getDate("data_ending"),
                    resultSet.getInt("payment_id"), resultSet.getInt("realtor_id"), resultSet.getInt("operation_id")));
        }
        logger.info(contractList);
        return false;
    }

}
