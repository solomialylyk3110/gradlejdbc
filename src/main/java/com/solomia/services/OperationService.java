package com.solomia.services;

import com.solomia.connector.DBManager;
import com.solomia.model.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OperationService implements Service<Operation>{
    private static Logger logger = LogManager.getLogger(OperationService.class);
    private static final String CREATE = "INSERT INTO operation VALUES (?, ?)";
    private static final String UPDATE = "UPDATE operation SET operation_name=? WHERE operation_id=?";
    private static final String DELETE = "DELETE FROM operation WHERE operation_id=?";
    private static final String DELETEBYCONTRACT = "DELETE FROM operation WHERE contract_id=?";
    private static final String GET_ALL = "SELECT * FROM operation";
    private static final String SELECT = "SELECT * FROM operation where operation_id=?";

    @Override
    public void create(Operation entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getOperationId());
            ps.setString(2,entity.getOperationName());
            ps.executeUpdate();
        }
    }

    public void update(Operation entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getOperationId());
            ps.setString(2,entity.getOperationName());
            ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteByContract(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYCONTRACT)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public boolean selectById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(SELECT);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        List<Operation> operationList = new ArrayList<>();
        while (resultSet.next()) {
            operationList.add(new Operation(resultSet.getInt("operation_id"),
                    resultSet.getString("operation_name")));
        }
        logger.info(operationList);
        return false;
    }

    @Override
    public void select() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(GET_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Operation> operationList = new ArrayList<>();
        while (resultSet.next()) {
            operationList.add(new Operation(resultSet.getInt("operation_id"),
                    resultSet.getString("operation_name")));
        }
        logger.info(operationList);
    }
}
