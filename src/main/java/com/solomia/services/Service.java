package com.solomia.services;

import java.sql.SQLException;

public interface Service<T> { //DAO
    void create(T t) throws SQLException;
    void select() throws SQLException; //read
    void update(T t) throws SQLException;
    void delete(int id) throws SQLException;
    boolean selectById(int id) throws SQLException;
}
